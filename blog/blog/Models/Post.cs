﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace blog.Models
{
    public class Post
    {
        [Key]
        public int Id { set; get; }
        public String Title {set; get;}
        public String Body { set; get; }

    }
    public class blogDBContext : DbContext
    {
        public DbSet<Post> Posts { set; get; }

    }
}