namespace MvcApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan21 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "DataCreated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "DataCreated", c => c.String());
        }
    }
}
