﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DataCreated { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}