﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        public int ID { set; get; }
        [DataType(DataType.Password)]
        public string PASSWORD { set; get; }
        [DataType(DataType.EmailAddress)]
        public string Email { set; get; }
        [StringLength(100, ErrorMessage = ("So ky tu tu 10 den 100"), MinimumLength = 10)]
        public string LastName { set; get; }
        [StringLength(100, ErrorMessage = ("So ky tu tu 10 den 100"), MinimumLength = 10)]
        public string FirtName { set; get; }


        public virtual ICollection<Post> Posts { set; get; }
    }
}