﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        public DateTime DateCreates { set; get; }
        public DateTime DateUpdates { set; get; }
        public string Author { set; get; }
        public int PostID { set; get; }
        public virtual Post Posts { set; get; }
    }
}