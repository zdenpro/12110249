﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        public string Title { set; get; }
        [StringLength(250, ErrorMessage = ("So luong ky tu trong khoang tu 10 den 250"), MinimumLength = 10)]
        public string Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateCreates { set; get; }
        public DateTime DateUpdates { set; get; }

        public virtual ICollection<Comment> comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

        public int AccountID { set; get; }
        public virtual Account Accounts { set; get; }
    }
}